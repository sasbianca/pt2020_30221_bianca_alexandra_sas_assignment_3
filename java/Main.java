
import businessLayer.Controller;
import com.itextpdf.text.DocumentException;

import java.io.IOException;
import java.sql.SQLException;

public class Main {

    public static void main(String[] argv)  {

        Controller controller = new Controller();
        try{
            controller.readFile(argv[0]);
        }
       catch(IOException e) {
            e.printStackTrace();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e){
            e.printStackTrace();
        }
        catch (DocumentException e){
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
