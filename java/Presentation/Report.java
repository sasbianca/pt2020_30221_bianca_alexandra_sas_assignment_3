package Presentation;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dataAccessLayer.ConnectionFactory;
import dataAccessLayer.Queries;
import model.Order;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Clasa contine fct de realizat documente pdf.
 */
public class Report {

    /**
     * Fct genereaza un document pdf cu tabela data ca parametru
     * @param className numele clasei care modeleaza tabela
     * @param tableName numele tabelei
     * @throws SQLException
     * @throws FileNotFoundException
     * @throws DocumentException
     * @throws ClassNotFoundException
     */
    public void executeReport(String className, String tableName) throws SQLException, FileNotFoundException, DocumentException, ClassNotFoundException {
        Connection connection = ConnectionFactory.getSingleInstance().getConnection();
        Statement statement = connection.createStatement();
        Queries queries = new Queries();

        Class obj = Class.forName(className);

        ResultSet resultSet = queries.selectAll("warehouse."+tableName,statement);

        int counter = 1;
        File temp = new File(tableName+counter+".pdf");
        while(temp.exists()){
            counter++;
            temp = new File(tableName+counter+".pdf");
        }

        Document document = new Document();
        PdfWriter.getInstance(document,new FileOutputStream(temp));
        document.open();

        Field[] fieldArray = obj.getDeclaredFields();
        PdfPTable table = new PdfPTable(fieldArray.length);

        for(Field field: fieldArray){
            PdfPCell header = new PdfPCell();
            header.setBackgroundColor(BaseColor.LIGHT_GRAY);
            header.setBorderWidth(2);
            header.setPhrase(new Phrase(field.getName()));
            table.addCell(header);
        }

        while(resultSet.next()){
            for(Field field: fieldArray){
                table.addCell(resultSet.getString(field.getName()));
            }
        }

        document.add(table);
        document.close();
        statement.close();
        resultSet.close();
        connection.close();

    }


    /**
     * Fct genereaza factura unei comenzi ca document pdf care contine numele clientelui, numele produsului, cantitatea si pretul comenzii.
     * @param obj comanda
     * @throws SQLException
     * @throws FileNotFoundException
     * @throws DocumentException
     */
    public void bill(Object obj) throws SQLException, FileNotFoundException, DocumentException {
        Connection connection =ConnectionFactory.getSingleInstance().getConnection();
        Statement statement = connection.createStatement();
        Queries queries = new Queries();

        ResultSet resultSet = queries.select(obj,statement);
        if(!resultSet.next()){
            System.out.println("gol");
        }

        int counter = 1;
        File temp = new File("bill"+counter+".pdf");
        while(temp.exists()){
            counter++;
            temp = new File("bill"+counter+".pdf");
        }

        Document document = new Document();
        PdfWriter.getInstance(document,new FileOutputStream(temp));
        document.open();
        PdfPTable table = new PdfPTable(4);

        String[] tableHeader = {"Client", "Product", "Quantity", "Price"};

        for(int i = 0; i < 4; i++){
            PdfPCell header = new PdfPCell();
            header.setBackgroundColor(BaseColor.LIGHT_GRAY);
            header.setBorderWidth(2);
            header.setPhrase(new Phrase(tableHeader[i]));
            table.addCell(header);
        }

        table.addCell(new Phrase(resultSet.getString("clientName")));
        table.addCell(new Phrase(resultSet.getString("productName")));
        table.addCell(new Phrase(resultSet.getString("quantity")));

        float price = (float) (((Order)obj).getQuantity()*queries.returnPrice(((Order)obj).getProductName(),statement));
        table.addCell(String.valueOf(price));
        document.add(table);
        document.close();
        statement.close();
        resultSet.close();
        connection.close();

    }


    /**
     * Fct genereaza un document pdf care contine un mesaj de eroare cand se incearca plasarea unei comenzi a unui produs cu stoc insuficient
     * @throws FileNotFoundException
     * @throws DocumentException
     */
    public void errorBill() throws FileNotFoundException, DocumentException {

        int counter = 1;
        File temp = new File("error"+counter+".pdf");
        while(temp.exists()){
            counter++;
            temp = new File("error"+counter+".pdf");
        }

        Document document = new Document();
        PdfWriter.getInstance(document,new FileOutputStream(temp));
        document.open();

        Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
        Chunk chunk = new Chunk("Not enough in stock", font);

        document.add(chunk);
        document.close();
    }
}
