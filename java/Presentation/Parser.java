package Presentation;

import java.io.*;
import java.util.Scanner;

public class Parser {
    /** Functia citeste din fisier si returneaza comanda ca un sir de stringuri
     *
     * @param line linia citita din fisierul de intrare
     * @return sirul de stringuri care reprezinta cuvinte cheie din comanda
     * @throws IOException
     */
    public String[] returnCommands(String line) throws IOException {
            String[] query;
            String[] commandLine = line.split(":");
            if(commandLine.length > 1) {
                String[] command = commandLine[0].split(" ");
                String[] attributes = commandLine[1].split(",");
                query = new String[command.length + attributes.length];
                for (int i = 0; i < command.length; i++) {
                    query[i] = command[i].trim();
                }
                for (int i = command.length; i < command.length + attributes.length; i++) {
                    query[i] = attributes[i - command.length].trim();
                }
            }
            else
                query = line.split(" ");
            return query;
    }

    /**
     * Fct valideaza comenziile citite din fisier
     * @param command comanda
     * @return returneaza 0 daca comanda e valida, -1 daca e gresita
     */
    public int validator(String[] command) {
        if (command[0].equals("Insert")) {
            if(command.length < 4){
                System.out.println("Command word no invalid");
                return -1;
            }
            if (command[1].equals("client")){
                if(command[2].length()==0){
                    System.out.println("Insert client command isn't valid, null name");
                    return -1;
                }
                if(command[3].length()==0){
                    System.out.println("Insert client command isn't valid, null address");
                    return -1;
                }
            }
            if(command[1].equals("product")){
                if(command[2].length()==0){
                    System.out.println("Insert product command isn't valid, null name");
                    return -1;
                }
                if(Integer.parseInt(command[3]) <= 0){
                    System.out.println("Insert product command isn't valid, quantity negative or zero");
                    return -1;
                }
                if(Float.parseFloat(command[4]) < 0){
                    System.out.println("Insert product command isn't valid, price negative");
                    return -1;
                }
            }
        }
        if(command[0].equals("Delete")){
            if(command.length != 3)
            if(command[1].equals("client")){
                if(command[2].length()==0){
                    System.out.println("Delete client command isn't valid, null name");
                    return -1;
                }
            }
            if(command[1].equals("product")){
                if(command[2].length()==0){
                    System.out.println("Delete product command isn't valid, null name");
                    return -1;
                }
            }
        }
        if(command[0].equals("Order")){
            if(command.length != 4){
                System.out.println("Command word no invalid");
                return -1;
            }
            if(command[1].length()==0){
                System.out.println("Order command isn't valid, null client name");
                return -1;
            }
            if(command[2].length()==0){
                System.out.println("Order command isn't valid, null product name");
                return -1;
            }
            if(Integer.parseInt(command[3]) <= 0){
                System.out.println("Order command isn't valid, quantity negative or zero");
                return -1;
            }
        }
        return 0;
    }


}
