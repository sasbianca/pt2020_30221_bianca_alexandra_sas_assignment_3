package businessLayer;

import Presentation.Parser;
import Presentation.Report;
import com.itextpdf.text.*;
import dataAccessLayer.ConnectionFactory;
import dataAccessLayer.Queries;
import model.Client;
import model.Order;
import model.Product;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Clasa Controller contine metode de citire din fisier si executare a operatiilor
 */
public class Controller {
    /**
     * Functia citeste din fisier linie cu linie
     * @param fileName numele fisierului de intrare
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws DocumentException
     */
    public void readFile(String fileName) throws IOException, SQLException, ClassNotFoundException, DocumentException, NoSuchFieldException, IllegalAccessException {
        Parser parser = new Parser();
        File file = new File(fileName);
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            String[] command = parser.returnCommands(scanner.nextLine());
            if(parser.validator(command) == -1)
                continue;
            executeCommand(command);
        }
    }

    /**
     * Functia determina care functie se va executa in functie de comanda din parametru
     * @param command sir de stringuri in care se afla comanda si argumentele
     * @throws SQLException
     * @throws FileNotFoundException
     * @throws DocumentException
     * @throws ClassNotFoundException
     */
    public void executeCommand(String[] command) throws SQLException, FileNotFoundException, DocumentException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException {

        switch (command[0]) {
            case "Insert":
                if(command[1].equalsIgnoreCase("client")) {
                    Client client = new Client(command[2], command[3]);
                    executeInsert(client);
                }
                if(command[1].equalsIgnoreCase("product")){
                    Product product = new Product(command[2],Integer.parseInt(command[3]),Float.parseFloat(command[4]));
                    executeInsert(product);
                }
                break;
            case "Delete":
                if(command[1].equalsIgnoreCase("client")){
                    Client client = new Client(command[2],null);
                    executeDelete(client);
                }
                if(command[1].equalsIgnoreCase("product")){
                    Product product = new Product(command[2],0,0);
                    executeDelete(product);
                }
                break;
            case "Order":
                Order order = new Order(command[1],command[2],Integer.parseInt(command[3]));
                executeOrder(order);
                break;
            case "Report":
                Report report = new Report();
                report.executeReport("model."+command[1].substring(0,1).toUpperCase()+command[1].substring(1),command[1]);
                break;
        }

    }

    /**
     * Fct insereaza in baza de date un client sau produs. Verifica daca a fost deja inserat in tabela
     * @param obj clientul sau produsul de inserat
     * @throws SQLException
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    public void executeInsert(Object obj) throws SQLException, NoSuchFieldException, IllegalAccessException {
        Connection connection = ConnectionFactory.getSingleInstance().getConnection();
        Statement statement = connection.createStatement();

        Queries queries = new Queries();
        ResultSet resultSet = queries.select(obj,statement);


        if(resultSet.next()){
            Class classObj = obj.getClass();
            if(classObj.getSimpleName().equals("Product")){
                Field f = classObj.getDeclaredField("quantity");
                f.setAccessible(true);
                int quantity = resultSet.getInt("quantity")+(Integer)f.get(obj);
                queries.update(obj,quantity);
            }
            else {
                System.out.println("Same client can't be added more than once");
            }
        }
        else {
            queries.insert(obj);
        }
        statement.close();
        resultSet.close();
        connection.close();
    }

    /**
     * Fct marcheaza un client sau produs ca sters actualizand campul deleted. Verifica daca a fost sters deja.
     * @param obj clientul sau produsul de sters
     * @throws SQLException
     */
    public void executeDelete(Object obj) throws SQLException {
        Connection connection =ConnectionFactory.getSingleInstance().getConnection();
        Statement statement = connection.createStatement();
        Queries queries = new Queries();

        ResultSet resultSet = queries.select(obj,statement);

        if(!resultSet.next()){
            System.out.println("Can't delete inexistent");
        }
        else {
            queries.delete(obj);
        }
        statement.close();
        resultSet.close();
        connection.close();
    }

    /**
     * Fct plaseaza o comanda inserand in tabela order comanda. Daca e deja inserata se actualizeaza cantitatea. Se genereaza factura.
     * @param obj comanda
     * @throws SQLException
     * @throws FileNotFoundException
     * @throws DocumentException
     */
    public void executeOrder(Object obj) throws SQLException, FileNotFoundException, DocumentException {
        Connection connection =ConnectionFactory.getSingleInstance().getConnection();
        Statement statement = connection.createStatement();
        Queries queries = new Queries();
        Report report = new Report();

        ResultSet resultSet = queries.select(new Client(((Order)obj).getClientName(),null),statement);

        if(!resultSet.next()){
            System.out.println("Can't make order for inexistent client");
            statement.close();
            resultSet.close();
            connection.close();
            return;
        }
        resultSet.close();

        resultSet = queries.select(new Product(((Order)obj).getProductName(),0, (float) 0.0),statement);

        if(!resultSet.next()){
            System.out.println("Can't make order for inexistent product");
            statement.close();
            resultSet.close();
            connection.close();
            return;
        }
        else
        {   int quantity = resultSet.getInt("quantity") - ((Order)obj).getQuantity();
            if(quantity >= 0){
              queries.update(new Product(((Order) obj).getProductName(),0,0),quantity);
            }
            else {
                report.errorBill();
                statement.close();
                resultSet.close();
                connection.close();
                return;
            }
        }
        resultSet.close();

        resultSet = queries.select(obj,statement);

        if(resultSet.next()){
            int quantity = resultSet.getInt("quantity") + ((Order)obj).getQuantity();
            queries.update(obj,quantity);
            report.bill(obj);
        }
        else {
           queries.insert(obj);
           report.bill(obj);
        }
        statement.close();
        resultSet.close();
        connection.close();
    }









}
