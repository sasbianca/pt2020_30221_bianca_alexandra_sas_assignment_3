package model;

/**
 * Modeleaza comanda. Atributele sunt campurile din tabela order din baza de date. Folosita pt reflexie
 */
public class Order implements DataBase {
    private String clientName;
    private String productName;
    private int quantity;

    public Order(String clientName, String productName, int quantity){
        this.clientName = new String(clientName);
        this.productName = new String(productName);
        this.quantity = quantity;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Fct returneaza constrangerea care va fi folosita ca parte a interogarilor
     * @return conditia cheii primare. Respecta sintaxa sql, poate fi folosit in interogari
     */
    @Override
    public String returnPrimaryKey() {
        return "clientName=\""+getClientName()+"\" and productName=\""+getProductName()+"\"";
    }

    /**
     * Fct returneaza numele tabelei
     * @return numele tabelei
     */
    @Override
    public String returnTableName() {
        return "warehouse.order";
    }

    public String toString(){
        return "(\""+clientName+"\",\""+quantity+"\",\""+productName+"\",\"0\")";
    }
}
