package model;

import java.util.ArrayList;

/**
 * Modeleaza clientul. Atributele sunt campurile din tabela client din baza de date. Folosita pt reflexie
 */
public class Client implements DataBase {
    private String name;
    private String address;

    public Client(String name, String address){
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Fct returneaza constrangerea care va fi folosita ca parte a interogarilor
     * @return conditia cheii primare. Respecta sintaxa sql, poate fi folosit in interogari
     */
    @Override
    public String returnPrimaryKey() {
        return "name=\""+getName()+"\"";
    }

    /**
     * Fct returneaza numele tabelei
     * @return numele tabelei
     */
    @Override
    public String returnTableName() {
        return "warehouse.client";
    }

    public String toString(){
        return "(\""+name+"\",\""+address+"\",\"0\")";
    }


}
