package model;

/**
 * Interfata implementata de toate clasele care modeleaza baza de date.
 */
public interface DataBase {

    public String returnPrimaryKey();
    public String returnTableName();

}
