package model;

/**
 * Modeleaza produsul. Atributele sunt campurile din tabela product din baza de date. Folosita pt reflexie
 */
public class Product implements DataBase {
    private String name;
    private int quantity;
    private float price;

    public Product(String name, int quantity, float price){
        this.name = new String(name);
        this.quantity = quantity;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    /**
     * Fct returneaza constrangerea care va fi folosita ca parte a interogarilor
     * @return conditia cheii primare. Respecta sintaxa sql, poate fi folosit in interogari
     */
    @Override
    public String returnPrimaryKey() {
        return "name=\""+getName()+"\"";
    }

    /**
     * Fct returneaza numele tabelei
     * @return numele tabelei
     */
    @Override
    public String returnTableName() {
        return "warehouse.product";
    }

    public String toString(){
        return "(\""+name+"\",\""+quantity+"\",\""+price+"\",\"0\")";
    }
}
