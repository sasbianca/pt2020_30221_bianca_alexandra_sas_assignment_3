package dataAccessLayer;

import model.DataBase;

import java.sql.*;

/**
 * Clasa Queries contine fct care executa interogari in baza de dat
 */
public class Queries {
    /**
     * Fct face select pe tabela obiectului dat, cautand dupa cheie primara obiectul
     * @param obj obiectul cautat
     * @param statement
     * @return returneaza ResultSet, rezultatul interogarii
     * @throws SQLException
     */
    public ResultSet select(Object obj, Statement statement) throws SQLException {
        String selectQuery;

        selectQuery = "select * from "+((DataBase)obj).returnTableName()+" where "+((DataBase)obj).returnPrimaryKey()+" and deleted=\"0\""+";";

        ResultSet resultSet = statement.executeQuery(selectQuery);
        return resultSet;
    }

    /**
     * Fct face select pe tabela a carui nume e dat ca parametru
     * @param tableName numele tabelei
     * @param statement statement trebuie generat de o conexiune inainte de apelul fct
     * @return returneaza ResultSet, rezultatul interogarii
     * @throws SQLException
     */
    public ResultSet selectAll(String tableName, Statement statement) throws SQLException {
        String selectQuery;
        selectQuery = "select * from "+tableName+" where deleted=\"0\";";
        ResultSet resultSet = statement.executeQuery(selectQuery);
        return resultSet;
    }

    /**
     * Fct insereaza in tabela obiectul dat.
     * @param obj
     * @throws SQLException
     */
    public void insert(Object obj) throws SQLException {
        Connection connection = ConnectionFactory.getSingleInstance().getConnection();
        Statement statement = connection.createStatement();
        String query = "insert into "+((DataBase)obj).returnTableName()+" values "+ obj.toString()+";";
        statement.execute(query);
        statement.close();
        connection.close();
    }

    /**
     * Fct marcheaza in tabela obiectul ca sters, actualizand campul deleted
     * @param obj
     * @throws SQLException
     */
    public void delete(Object obj) throws SQLException {
        Connection connection = ConnectionFactory.getSingleInstance().getConnection();
        Statement statement = connection.createStatement();
        String query = "update "+((DataBase)obj).returnTableName()+" set deleted=\"1\" where "+((DataBase)obj).returnPrimaryKey()+";";
        statement.execute(query);
        statement.close();
        connection.close();
    }

    /**
     * Fct actualizeaza campul quantity a unui produs sau a unei comenzi
     * @param obj
     * @param quantity
     * @throws SQLException
     */
    public void update(Object obj, int quantity) throws SQLException {
        Connection connection = ConnectionFactory.getSingleInstance().getConnection();
        Statement statement = connection.createStatement();
        String query = "update "+((DataBase)obj).returnTableName()+" set quantity="+quantity+" where "+((DataBase)obj).returnPrimaryKey()+";";
        statement.execute(query);
        statement.close();
        connection.close();
    }

    /**
     * Fct returneaza pretul produsului dat ca parametru
     * @param productName numele produsului
     * @param statement statement trebuie generat de o conexiune inainte de apelul fct
     * @return returneaza pretul ca float
     * @throws SQLException
     */
    public float returnPrice(String productName, Statement statement) throws SQLException {
        String queryPrice = "select * from warehouse.product where name=\""+productName+"\";";
        ResultSet resultPrice = statement.executeQuery(queryPrice);
        resultPrice.next();
        float price = resultPrice.getFloat("price");
        return price;
    }


}
