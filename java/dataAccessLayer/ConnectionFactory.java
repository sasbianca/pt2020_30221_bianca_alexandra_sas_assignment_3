package dataAccessLayer;

import java.sql.*;
import java.util.logging.Logger;

/**
 * Clasa singleton. Genereaza conexiuni
 */
public class ConnectionFactory {
    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/warehouse";
    private static final String USER = "root";
    private static final String PASS = "oscar1000";
    private static ConnectionFactory singleInstance = new ConnectionFactory();

    /**
     * Constructor
     */
    private ConnectionFactory(){
        try{
            Class.forName(DRIVER);
        }
        catch(ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    /**
     * Creeaza o conexiune
     * @return conexiunea
     */
    private Connection createConnection(){
        try
        {
            return DriverManager.getConnection(DBURL,USER,PASS);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return  null;
    }

    /**
     * Returneaza conexiunea
     * @return conexiunea
     */
    public Connection getConnection(){
        return createConnection();
    }

    public static ConnectionFactory getSingleInstance() {
        return singleInstance;
    }
}
